package com.ceodev18.mediblock_android.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ceodev18.mediblock_android.R;
import com.ceodev18.mediblock_android.models.Hospital;
import com.ceodev18.mediblock_android.ui.activities.menu.DetailCenterActivity;
import com.google.gson.Gson;

import java.util.List;

public class HospitalAdapters extends RecyclerView.Adapter<HospitalViewHolder> {
    private Context context;
    private List<Hospital> list;

    public HospitalAdapters(Context context,List<Hospital> list){
        this.context = context;
        this.list = list;
    }
    @NonNull
    @Override
    public HospitalViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_hospital, parent, false);
        return new HospitalViewHolder(view);

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public void onBindViewHolder(@NonNull HospitalViewHolder holder, final int position) {
        holder.tv_name.setText(list.get(position).getName());
        holder.tv_date.setText("Fecha de Afiliación: "+list.get(position).getAgreemnent_date());
        holder.tv_center.setText(list.get(position).getCategory());
        holder.cv_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, DetailCenterActivity.class);
                Gson gson = new Gson();
                intent.putExtra("hospital",gson.toJson(list.get(position)));
                context.startActivity(intent);
            }
        });
    }


}
