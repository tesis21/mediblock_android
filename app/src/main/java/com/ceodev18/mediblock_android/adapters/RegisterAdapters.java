package com.ceodev18.mediblock_android.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ceodev18.mediblock_android.R;
import com.ceodev18.mediblock_android.models.Hospital;
import com.ceodev18.mediblock_android.models.Register;

import java.util.List;


public class RegisterAdapters extends RecyclerView.Adapter<RegisterViewHolder> {
    private Context context;
    private List<Register> list;

    public RegisterAdapters(Context context,List<Register> list){
        this.context = context;
        this.list = list;
    }
    @NonNull
    @Override
    public RegisterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_register, parent, false);
        return new RegisterViewHolder(view);

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public void onBindViewHolder(@NonNull RegisterViewHolder holder, int position) {
        holder.tv_center.setText("Centro:" +list.get(position).getCentro());
        holder.tv_date.setText("Fecha: "+list.get(position).getDate());
        holder.tv_type.setText(list.get(position).getType());
        holder.iv_content.setImageDrawable(list.get(position).getImg());
    }


}
