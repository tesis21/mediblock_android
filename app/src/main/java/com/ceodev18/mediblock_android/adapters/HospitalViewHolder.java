package com.ceodev18.mediblock_android.adapters;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.ceodev18.mediblock_android.R;

public class HospitalViewHolder extends RecyclerView.ViewHolder{
    public TextView tv_name, tv_date, tv_center;
    public CardView cv_main;

    public HospitalViewHolder(@NonNull View itemView) {
        super(itemView);
        tv_name = itemView.findViewById(R.id.tv_name);
        tv_date = itemView.findViewById(R.id.tv_date);
        tv_center = itemView.findViewById(R.id.tv_center);
        cv_main = itemView.findViewById(R.id.cv_main);
    }
}
