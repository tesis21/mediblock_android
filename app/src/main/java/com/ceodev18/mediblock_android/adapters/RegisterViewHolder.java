package com.ceodev18.mediblock_android.adapters;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ceodev18.mediblock_android.R;

public class RegisterViewHolder extends RecyclerView.ViewHolder{
    public TextView tv_type;
    public TextView tv_center;
    public TextView tv_date;
    public ImageView iv_content;;
    public RegisterViewHolder(@NonNull View itemView) {
        super(itemView);
        tv_type = itemView.findViewById(R.id.tv_type);
        tv_center = itemView.findViewById(R.id.tv_center);
        tv_date = itemView.findViewById(R.id.tv_date);
        iv_content = itemView.findViewById(R.id.iv_content);

    }
}




