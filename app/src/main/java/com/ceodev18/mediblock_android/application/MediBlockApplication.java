package com.ceodev18.mediblock_android.application;


import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.os.Build;

public class MediBlockApplication {

    public static final String CHANNEL_1_ID  = "inverial_application";
    public static final String CHANNEL_DESC = "Firebase Cloud Messaging";
    private static MediBlockApplication mediBlockApplication;

    public static MediBlockApplication getInstance (){
        if (mediBlockApplication == null) mediBlockApplication = new  MediBlockApplication();
        return mediBlockApplication;
    }
    /*@Override
    public void onCreate() {
        super.onCreate();
        createNotificationChannels();
    }*/


    public void createNotificationChannels(Context context){

        if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.O){
            NotificationChannel channel = new NotificationChannel(
                    CHANNEL_1_ID,
                    "Channel Bauen",
                    NotificationManager.IMPORTANCE_HIGH);
            channel.setDescription("Este es el canal de Bauen");

            NotificationManager manager = context.getSystemService(NotificationManager.class);
            manager.createNotificationChannel(channel);
        }

    }


}