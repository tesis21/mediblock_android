package com.ceodev18.mediblock_android.core.login;

import okhttp3.ResponseBody;

public interface LoginView {
    void emptyUsername();
    void validUsername();
    void emptyPassword();
    void validPassword();
    void loginSuccess();
    void showProgress();
    void hideProgress();
    void showError(ResponseBody responseBody);
    void errorConnection();
}
