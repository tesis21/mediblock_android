package com.ceodev18.mediblock_android.core.register;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.ceodev18.mediblock_android.core.login.LoginPresenter;
import com.ceodev18.mediblock_android.core.login.LoginView;
import com.ceodev18.mediblock_android.models.views.Login;
import com.ceodev18.mediblock_android.models.views.LoginBody;
import com.ceodev18.mediblock_android.models.views.RegisterBody;
import com.ceodev18.mediblock_android.models.views.RegisterUserResponse;
import com.ceodev18.mediblock_android.models.views.TokenBody;
import com.ceodev18.mediblock_android.ui.activities.MenuActivity;
import com.ceodev18.mediblock_android.utils.communication.BlockMedCallBack;
import com.ceodev18.mediblock_android.utils.communication.RestClient;
import com.ceodev18.mediblock_android.utils.communication.RestView;
import com.ceodev18.mediblock_android.utils.persistence.SharedPreferencesManager;

import retrofit2.Call;
import retrofit2.Response;

public class RegisterInteractor  implements RegisterPresenter {
    private String TAG = "LoginInteractor LOG";
    private String ERROR_REGISTER = "Erro al registrar cuenta";
    private Context context;
    private RegisterView registerView;
    private SharedPreferencesManager sharedPreferencesManager;

    public RegisterInteractor(RegisterView registerView, Context context){
        this.registerView = registerView;
        this.context = context;

    }


    @Override
    public void registerUser(final RegisterBody registerBody) {
        sharedPreferencesManager = SharedPreferencesManager.getInstance(context);
        registerView.showProgress();
        Call<RestView<RegisterUserResponse>> call = new RestClient().getWebServices().register(registerBody);
        call.enqueue(new BlockMedCallBack<RestView<RegisterUserResponse>>(){
            @Override
            public void onResponse(Call<RestView<RegisterUserResponse>> call, Response<RestView<RegisterUserResponse>> response) {
                super.onResponse(call, response);

                RegisterUserResponse registerUserResponse = response.body().getBody();

                if(response.code() == 200){
                    //Toast.makeText(context, "200: ", Toast.LENGTH_SHORT).show()
                    //TODO do Login again
                    doLogin(registerBody.getUsername(),registerBody.getPassword());

                }else if(response.code()==403){
                    registerView.hideProgress();
                    Toast.makeText(context, ERROR_REGISTER, Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<RestView<RegisterUserResponse>> call, Throwable t) {
                super.onFailure(call, t);
                registerView.hideProgress();
                Toast.makeText(context, ERROR_REGISTER, Toast.LENGTH_SHORT).show();
            }
        });

    }
    private void doLogin(String username, String password){
        Call<RestView<LoginBody>> call = new RestClient().getWebServices().login(new Login(username,password));
        call.enqueue(new BlockMedCallBack<RestView<LoginBody>>(){
            @Override
            public void onFailure(Call<RestView<LoginBody>> call, Throwable t) {
                super.onFailure(call, t);
                registerView.hideProgress();
                Toast.makeText(context, "Erro de Autenticación", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onResponse(Call<RestView<LoginBody>> call, Response<RestView<LoginBody>> response) {
                super.onResponse(call, response);
                registerView.hideProgress();
                LoginBody loginBody = response.body().getBody();

                if(response.code() == 200 && loginBody!=null){
                    //Toast.makeText(context, "200: ", Toast.LENGTH_SHORT).show()
                    sharedPreferencesManager.saveToken(loginBody.getToken());
                    sharedPreferencesManager.saveUser(loginBody.getUser());
                    updateToken(loginBody.getUser().getId());
                    context.startActivity(new Intent(context, MenuActivity.class));
                }else if(response.code()==403){
                    Toast.makeText(context, "Erro de Autenticación", Toast.LENGTH_SHORT).show();
                    registerView.errorConnection();
                }
            }
        });

    }

    private void updateToken(int user_id){

        TokenBody tokenBody = new TokenBody();
        tokenBody.setToken(sharedPreferencesManager.getTokenMobile());
        tokenBody.setUser_id(user_id);
        Log.e(TAG,tokenBody.toString());
        String bearer  = "Bearer "+sharedPreferencesManager.getToken();
        Call<RestView> call = new RestClient().getWebServices().updateToken(bearer,tokenBody);
        call.enqueue(new BlockMedCallBack<RestView>(){
            @Override
            public void onFailure(Call<RestView> call, Throwable t) {
                super.onFailure(call, t);
            }

            @Override
            public void onResponse(Call<RestView> call, Response<RestView> response) {
                super.onResponse(call, response);
            }
        });
    }
}
