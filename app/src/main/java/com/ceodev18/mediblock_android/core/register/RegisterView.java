package com.ceodev18.mediblock_android.core.register;

import okhttp3.ResponseBody;

import okhttp3.ResponseBody;
public interface RegisterView {
    void emptyUsername();
    void validUsername();
    void emptyPassword();
    void validPassword();
    void loginSuccess();
    void showProgress();
    void hideProgress();
    void showError(ResponseBody responseBody);
    void errorConnection();
}
