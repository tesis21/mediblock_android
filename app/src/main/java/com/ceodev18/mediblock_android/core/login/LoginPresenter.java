package com.ceodev18.mediblock_android.core.login;

public interface LoginPresenter {
    void performLogin(String username, String password);
}
