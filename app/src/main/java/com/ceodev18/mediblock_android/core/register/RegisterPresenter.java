package com.ceodev18.mediblock_android.core.register;

import com.ceodev18.mediblock_android.models.views.RegisterBody;

public interface RegisterPresenter {
    void registerUser(RegisterBody registerBody);

}
