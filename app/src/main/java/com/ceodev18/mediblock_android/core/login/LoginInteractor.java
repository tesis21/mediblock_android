package com.ceodev18.mediblock_android.core.login;


import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.ceodev18.mediblock_android.models.User;
import com.ceodev18.mediblock_android.models.views.Login;
import com.ceodev18.mediblock_android.models.views.LoginBody;
import com.ceodev18.mediblock_android.models.views.TokenBody;
import com.ceodev18.mediblock_android.ui.activities.MenuActivity;
import com.ceodev18.mediblock_android.utils.communication.BlockMedCallBack;
import com.ceodev18.mediblock_android.utils.communication.RestClient;
import com.ceodev18.mediblock_android.utils.communication.RestView;
import com.ceodev18.mediblock_android.utils.persistence.SharedPreferencesManager;




import retrofit2.Call;
import retrofit2.Response;


public class LoginInteractor implements LoginPresenter {
    String TAG = "LoginInteractor LOG";
    private Context context;
    private LoginView loginView;
    private SharedPreferencesManager sharedPreferencesManager;


    public LoginInteractor(LoginView loginView, Context context){
        this.loginView = loginView;
        this.context = context;
    }

    @Override
    public void performLogin(String username, String password) {
        loginView.showProgress();
        login(username,password);

    }

    private void login(String username, String password){

        sharedPreferencesManager = SharedPreferencesManager.getInstance(context);
        Login login = new Login(username,password);

        Call<RestView<LoginBody>> call = new RestClient().getWebServices().login(login);
        call.enqueue(new BlockMedCallBack<RestView<LoginBody>>(){
            @Override
            public void onFailure(Call<RestView<LoginBody>> call, Throwable t) {
                super.onFailure(call, t);
                loginView.hideProgress();
                Toast.makeText(context, "Erro de Autenticación", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onResponse(Call<RestView<LoginBody>> call, Response<RestView<LoginBody>> response) {
                super.onResponse(call, response);
                loginView.hideProgress();
                LoginBody loginBody = response.body().getBody();

                if(response.code() == 200 && loginBody!=null){
                    //Toast.makeText(context, "200: ", Toast.LENGTH_SHORT).show()
                    sharedPreferencesManager.saveToken(loginBody.getToken());
                    sharedPreferencesManager.saveUser(loginBody.getUser());
                    updateToken(loginBody.getUser().getId());
                    context.startActivity(new Intent(context, MenuActivity.class));
                }else if(response.code()==403){
                    Toast.makeText(context, "Erro de Autenticación", Toast.LENGTH_SHORT).show();
                    loginView.errorConnection();
                }
            }
        });

    }

    private void updateToken(int user_id){

        TokenBody tokenBody = new TokenBody();
        tokenBody.setToken(sharedPreferencesManager.getTokenMobile());
        tokenBody.setUser_id(user_id);
        Log.e(TAG,tokenBody.toString());
        String bearer  = "Bearer "+sharedPreferencesManager.getToken();
        Call<RestView> call = new RestClient().getWebServices().updateToken(bearer,tokenBody);
        call.enqueue(new BlockMedCallBack<RestView>(){
            @Override
            public void onFailure(Call<RestView> call, Throwable t) {
                super.onFailure(call, t);
            }

            @Override
            public void onResponse(Call<RestView> call, Response<RestView> response) {
                super.onResponse(call, response);
            }
        });
    }
}