package com.ceodev18.mediblock_android.utils.notifications;
import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;


import androidx.core.app.NotificationCompat;

import com.ceodev18.mediblock_android.application.MediBlockApplication;
import com.ceodev18.mediblock_android.ui.activities.LoginActivity;
import com.ceodev18.mediblock_android.ui.activities.menu.permissions.ConfirmActivity;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.io.IOException;
import java.net.URL;
import java.util.Map;

import com.ceodev18.mediblock_android.R;

import static com.ceodev18.mediblock_android.utils.notifications.MyFirebaseMessagingService.FCM_PARAM;

@SuppressLint("Registered")
public class NotificationsBuilder {
    private Context context;
    private Map<String, String> data;
    private RemoteMessage.Notification notification;


    public NotificationsBuilder(Context context, RemoteMessage.Notification notification, Map<String, String> data){
        this.context = context;
        this.data = data;
        this.notification = notification;
    }
    public void notificationReleasedProject(){
        Intent intent = new Intent(context, ConfirmActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent,
                PendingIntent.FLAG_ONE_SHOT);
        NotificationManager notificationManager = createNotificationManager();
        NotificationCompat.Builder builder  = createBuilder();
        builder.setContentIntent(pendingIntent);
        notificationManager.notify(0, builder.build());
    }
    public  NotificationCompat.Builder createBuilder(){

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context, context.getString(R.string.notification_channel_id))
                .setContentTitle(notification.getTitle())
                .setContentText(notification.getBody())
                .setAutoCancel(true)
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                //.setSound(Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.win))
                //.setContentIntent(pendingIntent)
                .setContentInfo("Hello")
                .setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_launcher))
                .setColor(context.getResources().getColor(R.color.colorAccent))
                .setLights(Color.RED, 1000, 300)
                .setDefaults(Notification.DEFAULT_VIBRATE)
                // .setNumber(++numMessages)
                .setSmallIcon(R.mipmap.ic_launcher);

        try {
            String picture = data.get(FCM_PARAM);
            if (picture != null && !"".equals(picture)) {
                URL url = new URL(picture);
                Bitmap bigPicture = BitmapFactory.decodeStream(url.openConnection().getInputStream());
                notificationBuilder.setStyle(
                        new NotificationCompat.BigPictureStyle().bigPicture(bigPicture).setSummaryText(notification.getBody())
                );
            }
            NotificationManager notificationManager  = createNotificationManager();
            notificationManager.notify(0, notificationBuilder.build()); //.notify(0, notificationBuilder.build());
            return notificationBuilder;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public  NotificationManager createNotificationManager(){
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(
                    context.getString(R.string.notification_channel_id), MediBlockApplication.CHANNEL_1_ID, NotificationManager.IMPORTANCE_HIGH
            );
            channel.setDescription(MediBlockApplication.CHANNEL_DESC);
            channel.setShowBadge(true);
            channel.canShowBadge();
            channel.enableLights(true);
            channel.setLightColor(Color.RED);
            channel.enableVibration(true);
            channel.setVibrationPattern(new long[]{100, 200, 300, 400, 500});

            assert notificationManager != null;
            notificationManager.createNotificationChannel(channel);
        }

        assert notificationManager != null;
        return notificationManager;
        //notificationManager.notify(0, notificationBuilder.build());
    }
}
