package com.ceodev18.mediblock_android.utils;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Point;
import android.os.Build;
import android.util.Log;
import android.view.Display;
import android.view.WindowManager;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import com.ceodev18.mediblock_android.models.User;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;




public class CommonMethods {

    private static final int MIN_PASSWORD_LENGTH = 8;
    private static final int MAX_PASSWORD_LENGTH = 16;

    public static boolean isEmailValid(String email){
        String validator = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        Pattern pattern = Pattern.compile(validator, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }
    public static boolean validPasswords(String s1,String s2,int length){
        return (s1.equals(s2) && s1.length()>=length);
    }

    public static int digitsInNumber(int number){
        int length = String.valueOf(number).length();
        return length;
    }
    public static int rangeHours(String from, String to){
        String []start = from.split(":");
        String []end = to.split(":");

        return Integer.parseInt(end[0])-Integer.parseInt(start[0]);
    }


    public static String formatDate(int day, int month, int year){
        String strDay, strMonth;
        if (digitsInNumber(day) == 1){
            strDay = "0"+ day;
        }
        else{
            strDay = String.valueOf(day);
        }
        if (digitsInNumber(month) == 1){
            strMonth = "0"+ month;
        }
        else{
            strMonth = String.valueOf(month);
        }
        return (strDay + "/" + strMonth + "/" + year);
    }

    public static Boolean checkValidLengthOnPassword(String password){
        Boolean valid;
        valid = (password.length()<MIN_PASSWORD_LENGTH || password.length()>MAX_PASSWORD_LENGTH);
        return valid;
    }

    /*public static ProgressDialog createProgressDialog(Context activity, String message){
        final ProgressDialog progressDialog = new ProgressDialog(activity, R.style.MyAlertDialogStyle);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.setMessage(message);
        return progressDialog;
    }*/

    public static void  getPermissions(Activity activity, String[] PERMISSIONS, int code){
        Log.d("Permissions", "getPermissions: getting permissions");
        if(!checkPermissions(activity, PERMISSIONS)){
            ActivityCompat.requestPermissions(activity, PERMISSIONS, code);
        }
    }

    public static Boolean checkPermissions(Activity activity, String[] permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            if (activity != null && permissions != null){
                for (String permission : permissions){
                    if (ContextCompat.checkSelfPermission(activity.getApplicationContext(), permission)
                            != PackageManager.PERMISSION_GRANTED){
                        return false;
                    }
                }
            }
        }
        return true;
    }

    public static Date getNowDateWithouthTime(){
        Date currentDate=null;
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM-dd-yyyy");
            currentDate = simpleDateFormat.parse(simpleDateFormat.format(new Date()));
            return currentDate;
        } catch (ParseException e) {
            currentDate =  new Date();
            return currentDate;
        }
    }

    public static Calendar getNowDateCalendar(){
        Date currentDate = new Date();
        return getCalendarFromDateObject(currentDate);
    }

    public static Calendar getCalendarFromDateObject(Date date){
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal;
    }

    public static String getSpanishNameOfWeekDay(int d){
        switch (d){
            case 1: return "Dom";
            case 2: return "Lun";
            case 3: return "Mar";
            case 4: return "Mie";
            case 5: return "Jue";
            case 6: return "Vie";
            case 7: return "Sab";
            default: return "";
        }
    }

    public static String getSpanishCompleteNameOfWeekDay(int d){
        switch (d){
            case 1: return "Domingo";
            case 2: return "Lunes";
            case 3: return "Martes";
            case 4: return "Miercoles";
            case 5: return "Jueves";
            case 6: return "Viernes";
            case 7: return "Sabado";
            default: return "";
        }
    }

    public static String formatDate(Date date){
        return new SimpleDateFormat("dd/MM/YYYY").format(date);
    }

    public static String formatDay(String date){
        return date.length()==1?"0"+date:date;
    }
    public static int getHour(String time) {
        //This method recieves a time (HH:MM:SS) as a string and retrieves the hour (HH) as an int
        return Integer.valueOf(time.substring(0,2));
    }

    public static int getYear(String date) {
        return Integer.valueOf(date.substring(0,4));
    }

    public static int getMonth(String date){
        //Month must have 2 digits in date(March is '02' and not '2')
        return Integer.valueOf(date.substring(5,7));
    }

    public static int getDay(String date){
        //Day must have 2 digits in date(Use '09' and not '9')
        return Integer.valueOf(date.substring(8,10));
    }

    public static Date addDays(Date date, int days) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DATE, days); //minus number would decrement the days
        return cal.getTime();
    }

    public static String formatDouble(double number) {
        if((number-(int)number)!=0){
            return String.format("%.2f", number);
        }
        else
            return Integer.toString((int)number);
    }

    public static int getScreenWidth(Context context){
        WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = windowManager.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        return size.x;
    }

    public static String getDateText(Date date, int type) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/mm/yyyy");
        String bookingDate = simpleDateFormat.format(date.getTime());
        String weekDay = getSpanishCompleteNameOfWeekDay(getCalendarFromDateObject(date).get(Calendar.DAY_OF_WEEK));
        String result = "";
        if (type == 0)
            result = weekDay + " " + bookingDate;
        else if (type == 1)
            result = bookingDate + " (" + weekDay + ")";
        return result;
    }

    public static String getRangeOfHoursText(Date startDate, Date endDate) {
        int intInitialHour = getCalendarFromDateObject(startDate).get(Calendar.HOUR);
        if (intInitialHour == 0){
            intInitialHour = 12;
        }

        String initialHour = Integer.toString(intInitialHour);
        String endHour = Integer.toString(getCalendarFromDateObject(endDate).get(Calendar.HOUR) + 1);
        String amOrPm = "";
        if (getCalendarFromDateObject(endDate).get(Calendar.AM_PM) == 0){
            amOrPm = "AM";
        }
        else if ((getCalendarFromDateObject(endDate).get(Calendar.AM_PM) == 1)){
            amOrPm = "PM";
        }
        return (initialHour + " - " + endHour + " " + amOrPm);
    }





    public static String getFullName(User user) {
        return new String();
        //return user.getAttributes().getUser().getAttributes().getName() + " " + user.getAttributes().getUser().getAttributes().getLastname();
    }

    public static String getFirstWord(String str){
        return str.substring(str.indexOf(' ') + 1);
    }

    public static int getAge(String birthDateString) {
        int age = 0;
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-mm-dd");
        try {
            Date birthDate = formatter.parse(birthDateString);
            Calendar birthDateCalendar = CommonMethods.getCalendarFromDateObject(birthDate);
            age = calculateAge(birthDateCalendar);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return age;
    }

    private static int calculateAge(Calendar birthDateCalendar) {
        Calendar now = CommonMethods.getCalendarFromDateObject(new Date());

        if (birthDateCalendar.after(now)) {
            throw new IllegalArgumentException("Can't be born in the future");
        }
        int year1 = now.get(Calendar.YEAR);
        int year2 = birthDateCalendar.get(Calendar.YEAR);
        int age = year1 - year2;
        int month1 = now.get(Calendar.MONTH);
        int month2 = birthDateCalendar.get(Calendar.MONTH);
        if (month2 > month1) {
            age--;
        } else if (month1 == month2) {
            int day1 = now.get(Calendar.DAY_OF_MONTH);
            int day2 = birthDateCalendar.get(Calendar.DAY_OF_MONTH);
            if (day2 > day1) {
                age--;
            }
        }
        return age;
    }

    public static String getRangeOfHoursFromIntegers(int start, int end) {
        //This method receives startHour and endHour values in 24h format and retrieves it on (HH - HH AM/PM)
        if (start >= end){
            return "";
        }

        String amOrPm = "";
        if (start > 12){
            start -= 12;
        }

        if (end > 12){
            end -= 12;
            amOrPm = "PM";
        }

        return (start + " - " + end + " " + amOrPm);
    }

    public static String forceToEightCharacters(String str) {
        if (str.length() > 8){
            return str.substring(0,8);
        }

        if (str.length() < 8){
            int diff = 8 - str.length();
            StringBuilder stringBuilder = new StringBuilder(str);
            for (int i = diff; i > 0; i--){
                stringBuilder.append("0");
            }
            return stringBuilder.toString();
        }
        return str;
    }

    public static Date getDateFromHour(String strDate, int hour) {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.DAY_OF_MONTH, getDay(strDate));
        cal.set(Calendar.MONTH, getMonth(strDate)-1);
        cal.set(Calendar.YEAR, getYear(strDate));
        cal.set(Calendar.HOUR_OF_DAY, hour);
        return cal.getTime();
    }

    public static String parseDate(String date){
        if(date==null)return "";
        String[]list = date.split("-");
        String month = nameMonth(Integer.parseInt(list[1]));
        Integer day = Integer.parseInt(list[2]);
        return day+" de "+month+" del "+list[0];
    }

    public static String nameMonth(int month){
        switch (month){
            case 1: return "Enero";
            case 2: return "Febrero";
            case 3: return "Marzo";
            case 4: return "Abril";
            case 5: return "Mayo";
            case 6: return "Junio";
            case 7: return "Julio";
            case 8: return "Agosto";
            case 9: return "Septiembre";
            case 10: return "Octubre";
            case 11: return "Noviembre";
            case 12: return "Diciembre";
            default:return "";
        }

    }

    public static String parseTime(String time){
        if(time==null)return "";
        String[]list = time.split(":");

        String hour = list[0];
        String minute = list[1];

        Integer hour_i = Integer.parseInt(hour);
        if(hour_i<12){
            return hour+":"+minute+" AM";
        }else{
            return hour+":"+minute+" PM";
        }

    }


}
