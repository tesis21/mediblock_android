package com.ceodev18.mediblock_android.utils.communication;

import retrofit2.http.POST;

public interface Urls {
    String REFRESH_TOKEN = "user/refreshtoken";
    String LOGIN = "auth/login";
    String REGISTER = "register/patient";
    String UPDATE_TOKEN = "token";
    String INSTITUTIONS = "institutions/user";
    String COMPLETED_REGISTER = "player/{id}/complete-register";
    String RECOVER_ACCOUNT = "player/generate-password";
}

