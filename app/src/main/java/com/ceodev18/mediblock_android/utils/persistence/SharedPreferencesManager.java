package com.ceodev18.mediblock_android.utils.persistence;

import android.content.Context;
import android.content.SharedPreferences;

import com.ceodev18.mediblock_android.models.User;
import com.google.gson.Gson;

public class SharedPreferencesManager {
    private static final String PREFERENCES_NAME = "blcc";
    private static final String CURRENT_USER = "current_user";
    private static final String COMPLETED_REGISTER = "current_register";
    private static final String LAST_USERNAME_RECOVER = "current_username";
    private static final String TOKEN = "token";
    private static final String TOKEN_MOBILE = "token_mobile";

    private static final String PENDING_FRIENDSHIP = "pending_friendship";
    private static final String PENDING_EVENT = "pending_event";
    private static final String PENDING_TEAM = "pending_team";

    private static final String HISTORY_EVENTS = "history_events";

    private static SharedPreferencesManager self;
    private final SharedPreferences mPreferences;
    private Gson gson;
    public SharedPreferencesManager(Context context) {
        this.mPreferences = context.getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE);
        gson = new Gson();
    }

    public static SharedPreferencesManager getInstance(Context context){
        if (self == null){
            self = new SharedPreferencesManager(context);
        }
        return self;
    }


    public void closeSession(){
        SharedPreferences.Editor editor = mPreferences.edit();
        editor.putString(CURRENT_USER, null);
        editor.apply();
    }

    public User getUser(){
        String userString = mPreferences.getString(CURRENT_USER, "");
        if(!userString.isEmpty()){
            User user = gson.fromJson(userString, User.class);
            return user;
        }
        else{
            return null;
        }
    }



    public void saveToken(String token){
        SharedPreferences.Editor editor = mPreferences.edit();
        editor.putString(TOKEN, token);
        editor.apply();
    }
    public void saveTokenMobile(String token){
        SharedPreferences.Editor editor = mPreferences.edit();
        editor.putString(TOKEN_MOBILE, token);
        editor.apply();
    }
    public void saveUser(User user){
        SharedPreferences.Editor editor = mPreferences.edit();
        editor.putString(CURRENT_USER, gson.toJson(user));
        editor.apply();
    }

    public String getToken(){
        return mPreferences.getString(TOKEN, "");
    }
    public String getTokenMobile(){
        return mPreferences.getString(TOKEN_MOBILE, "");
    }



}
