package com.ceodev18.mediblock_android.utils.communication;

import com.ceodev18.mediblock_android.models.User;
import com.ceodev18.mediblock_android.models.views.InstitutionsBody;
import com.ceodev18.mediblock_android.models.views.InstitutionsResponse;
import com.ceodev18.mediblock_android.models.views.Login;
import com.ceodev18.mediblock_android.models.views.LoginBody;
import com.ceodev18.mediblock_android.models.views.RegisterBody;
import com.ceodev18.mediblock_android.models.views.RegisterUserResponse;
import com.ceodev18.mediblock_android.models.views.TokenBody;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface BlockMedServices {

    @POST(Urls.LOGIN)
    Call<RestView<LoginBody>> login(@Body Login login);

    @POST(Urls.REGISTER)
    Call<RestView<RegisterUserResponse>> register(@Body RegisterBody registerBody);

    @POST(Urls.UPDATE_TOKEN)
    Call<RestView> updateToken(@Header("Authorization") String authHeader, @Body TokenBody tokenBody);

    @POST(Urls.INSTITUTIONS)
    Call<RestView<List<InstitutionsResponse>>> getInstitutions(@Header("Authorization") String authHeader, @Body InstitutionsBody institutionsBody);

}
