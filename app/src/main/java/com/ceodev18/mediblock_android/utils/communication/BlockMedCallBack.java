package com.ceodev18.mediblock_android.utils.communication;

import android.util.Log;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.ceodev18.mediblock_android.utils.Globals.LOG_TAG;

public class BlockMedCallBack<T> implements Callback<T> {

    public BlockMedCallBack() {
    }

    @Override
    public void onResponse(Call<T> call, Response<T> response) {
        Log.d(LOG_TAG, String.valueOf(response));
    }

    @Override
    public void onFailure(Call<T> call, Throwable t) {
        Log.d(LOG_TAG, String.valueOf(t));
    }
}