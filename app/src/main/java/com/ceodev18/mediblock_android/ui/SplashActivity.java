package com.ceodev18.mediblock_android.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.ceodev18.mediblock_android.MainActivity;
import com.ceodev18.mediblock_android.R;
import com.ceodev18.mediblock_android.ui.activities.LoginActivity;
import com.ceodev18.mediblock_android.utils.persistence.SharedPreferencesManager;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import java.util.Timer;
import java.util.TimerTask;

import static com.ceodev18.mediblock_android.utils.Globals.SPLASH_TIME_OUT;


public class SplashActivity extends AppCompatActivity {
    private Activity self = this;
    private Context context = this;
    private SharedPreferencesManager sharedPreferencesManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        sharedPreferencesManager = SharedPreferencesManager.getInstance(context);
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(this,  new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                String newToken = instanceIdResult.getToken();
                Log.e("newToken",newToken);
                sharedPreferencesManager.saveTokenMobile(newToken);

            }
        });
        runSplash();
    }
    private void runSplash() {
        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                Intent intent = new Intent(self, LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK| Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        };
        Timer timer = new Timer();
        timer.schedule(task, SPLASH_TIME_OUT);
    }
}