package com.ceodev18.mediblock_android.ui.activities.menu.notifications;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.ceodev18.mediblock_android.R;
import com.ceodev18.mediblock_android.ui.activities.MenuActivity;
import com.ceodev18.mediblock_android.ui.activities.menu.contact.ContactActivity;
import com.ceodev18.mediblock_android.utils.persistence.SharedPreferencesManager;

import butterknife.BindView;
import butterknife.ButterKnife;

public class NotificationsFragment extends Fragment {

    private NotificationsViewModel notificationsViewModel;
    @BindView(R.id.et_name) TextView et_name;
    @BindView(R.id.et_email) TextView et_email;
    @BindView(R.id.et_phone) TextView et_phone;
    @BindView(R.id.tv_add_contact) TextView tv_add_contact;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        MenuActivity menuActivity = (MenuActivity)getActivity();
        menuActivity.setBg(getResources().getColor(R.color.colorPrimary));
        notificationsViewModel =
                new ViewModelProvider(this).get(NotificationsViewModel.class);
        View root = inflater.inflate(R.layout.fragment_notifications, container, false);
        ButterKnife.bind(this,root);
        loadContent();

        return root;
    }
    private void loadContent(){


        SharedPreferencesManager sharedPreferencesManager = SharedPreferencesManager.getInstance(getContext());
        et_name.setText(sharedPreferencesManager.getUser().getFirstName()+" "+sharedPreferencesManager.getUser().getLastName());
        et_email.setText(sharedPreferencesManager.getUser().getEmail());
        tv_add_contact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), ContactActivity.class);
                getContext().startActivity(intent);
            }
        });
    }
}