package com.ceodev18.mediblock_android.ui.activities.menu.home;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ceodev18.mediblock_android.R;
import com.ceodev18.mediblock_android.adapters.HospitalAdapters;
import com.ceodev18.mediblock_android.models.Hospital;
import com.ceodev18.mediblock_android.models.views.InstitutionsBody;
import com.ceodev18.mediblock_android.models.views.InstitutionsResponse;
import com.ceodev18.mediblock_android.models.views.LoginBody;
import com.ceodev18.mediblock_android.ui.activities.MenuActivity;
import com.ceodev18.mediblock_android.utils.communication.BlockMedCallBack;
import com.ceodev18.mediblock_android.utils.communication.RestClient;
import com.ceodev18.mediblock_android.utils.communication.RestView;
import com.ceodev18.mediblock_android.utils.persistence.SharedPreferencesManager;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Response;

public class HomeFragment extends Fragment {
    @BindView(R.id.rv_content)
    RecyclerView rv_content;
    private SharedPreferencesManager sharedPreferencesManager;
    private Context context;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_home, container, false);
        ButterKnife.bind(this, root);
        sharedPreferencesManager = SharedPreferencesManager.getInstance(getContext());
        context = getContext();
        setContent();
        return root;
    }

    private void setContent() {
        String bearer = "Bearer " + sharedPreferencesManager.getToken();

        Call<RestView<List<InstitutionsResponse>>> call = new RestClient().getWebServices().getInstitutions(bearer, new InstitutionsBody(sharedPreferencesManager.getUser().getId()));
        call.enqueue(new BlockMedCallBack<RestView<List<InstitutionsResponse>>>(){
            @Override
            public void onFailure(Call<RestView<List<InstitutionsResponse>>> call, Throwable t) {
                super.onFailure(call, t);
            }

            @Override
            public void onResponse(Call<RestView<List<InstitutionsResponse>>> call, Response<RestView<List<InstitutionsResponse>>> response) {
                super.onResponse(call, response);
                List<InstitutionsResponse> listx  = response.body().getBody();
                if(response.code() == 200 && listx!=null){
                    //Toast.makeText(context, "200: ", Toast.LENGTH_SHORT).show()
                    MenuActivity menuActivity = (MenuActivity) getActivity();
                    menuActivity.setBg(getResources().getColor(R.color.colorWhite));
                    List<Hospital> list = new ArrayList<>();
                    for(InstitutionsResponse ir : listx){
                        //list.add(new Hospital("09/07/2020", ir.getName(), 1, "Clínica"));
                        list.add(new Hospital(ir));
                    }

                    //list.add(new Hospital("09/07/2020", "Hospital San Juan", 1, "Hospital"));
                    //list.add(new Hospital("10/07/2020", "Hospital San Juan", 1, "Hospital"));
                    //list.add(new Hospital("11/07/2020", "Hospital San Ignacio", 1, "Hospital"));
                    //list.add(new Hospital("12/07/2020", "Limatambo", 1, "Clínica"));
                    //list.add(new Hospital("13/07/2020", "Javier Prado", 1, "Clínica"));

                    HospitalAdapters hospitalAdapters = new HospitalAdapters(getContext(), list);
                    rv_content.setHasFixedSize(true);
                    LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
                    rv_content.setLayoutManager(linearLayoutManager);
                    rv_content.setAdapter(hospitalAdapters);

                }else if(response.code()==403){
                    Toast.makeText(context, "Erro de Autenticación", Toast.LENGTH_SHORT).show();

                }
            }
        });

    }
}