package com.ceodev18.mediblock_android.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatEditText;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.ceodev18.mediblock_android.R;
import com.ceodev18.mediblock_android.core.login.LoginPresenter;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.ceodev18.mediblock_android.utils.CommonMethods.isEmailValid;
import static com.ceodev18.mediblock_android.utils.CommonMethods.validPasswords;

public class RegisterActivity extends AppCompatActivity {
    private Context context = this;

    @BindView(R.id.et_email) AppCompatEditText et_email;
    @BindView(R.id.et_password) AppCompatEditText et_password;
    @BindView(R.id.et_repassword) AppCompatEditText et_repassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);
    }

    private boolean is_validated(){
        String em = et_email.getText().toString();
        if(!isEmailValid(em)) {
            Toast.makeText(context, "Email no válido", Toast.LENGTH_SHORT).show();
            return false;
        }
        if(!validPasswords(et_password.getText().toString(),
                et_repassword.getText().toString(),8)) {
            Toast.makeText(context, "Verifique contraseñas", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }
    public void nextStep(View view) {
        //TODO move data to another view
        if(is_validated()){
            Intent intent= new Intent(context,CompleteRegisterActivity.class);
            intent.putExtra("username",et_email.getText().toString());
            intent.putExtra("password",et_password.getText().toString());
            startActivity(intent);
        }

    }

    public void kill(View view) {
        onBackPressed();
    }
}