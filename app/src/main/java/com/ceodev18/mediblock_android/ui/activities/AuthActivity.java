package com.ceodev18.mediblock_android.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatEditText;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.ceodev18.mediblock_android.R;
import com.ceodev18.mediblock_android.core.login.LoginPresenter;
import com.ceodev18.mediblock_android.core.login.LoginInteractor;
import com.ceodev18.mediblock_android.core.login.LoginView;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.ResponseBody;

public class AuthActivity extends AppCompatActivity  implements LoginView {
    private Activity self = this;
    private Context context = this;
    LoginPresenter loginPresenter;

    @BindView(R.id.et_username)
    AppCompatEditText et_username;

    @BindView(R.id.et_password)
    AppCompatEditText et_password;

    @BindView(R.id.pb_login)
    ProgressBar pb_login;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auth);
        ButterKnife.bind(this);
        loginPresenter = new LoginInteractor(this, context);
        hideProgress();
    }

    public void kill(View view) {
        onBackPressed();
    }

    public void doLogin(View view) {
        View viewx = this.getCurrentFocus();
        if (viewx != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
        showProgress();
        loginPresenter.performLogin(et_username.getText().toString(),et_password.getText().toString());
        //loginPresenter.performLogin("italo","12345678");
    }

    @Override
    public void emptyUsername() {

    }

    @Override
    public void validUsername() {

    }

    @Override
    public void emptyPassword() {

    }

    @Override
    public void validPassword() {

    }

    @Override
    public void loginSuccess() {
        pb_login.setVisibility(View.INVISIBLE);
    }

    @Override
    public void showProgress() {
        pb_login.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        pb_login.setVisibility(View.INVISIBLE);
    }

    @Override
    public void showError(ResponseBody responseBody) {

    }

    @Override
    public void errorConnection() {
        Toast.makeText(self, "Error de conexión", Toast.LENGTH_SHORT).show();
    }
}