package com.ceodev18.mediblock_android.ui.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.ceodev18.mediblock_android.R;
public class LoginActivity extends AppCompatActivity {
    private Activity self = this;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
    }

    public void doAuth(View view) {
        self.startActivity(new Intent(self,AuthActivity.class));
    }

    public void doSingUp(View view) { self.startActivity(new Intent(self,RegisterActivity.class)); }
}