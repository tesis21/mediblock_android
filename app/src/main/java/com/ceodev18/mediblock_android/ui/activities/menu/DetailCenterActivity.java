package com.ceodev18.mediblock_android.ui.activities.menu;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.TextView;

import com.ceodev18.mediblock_android.R;
import com.ceodev18.mediblock_android.adapters.RegisterAdapters;
import com.ceodev18.mediblock_android.models.Hospital;
import com.ceodev18.mediblock_android.models.Register;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DetailCenterActivity extends AppCompatActivity {
    Activity self = this;
    Hospital hospital;
    @BindView(R.id.tv_name) TextView tv_name;
    @BindView(R.id.rv_content) RecyclerView rv_content;
    @BindView(R.id.tv_email) TextView tv_email;
    @BindView(R.id.tv_address) TextView tv_address;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_center);
        // add back arrow to toolbar
        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        ButterKnife.bind(this);
        setContent();

    }
    private void setContent(){
        Gson gson = new Gson();
        hospital = gson.fromJson(getIntent().getStringExtra("hospital"),Hospital.class);
        tv_name.setText(hospital.getName());
        tv_email.setText(hospital.getEmail());
        tv_address.setText(hospital.getAddress());
        List<Register> list = new ArrayList<>();
        list.add(new Register("Cita Médica","Clínica Limatambo","29/07/202",getResources().getDrawable(R.drawable.ic_cita_med)));
        list.add(new Register("Registro Médico","Hospital San Juan","09/02/202",getResources().getDrawable(R.drawable.ic_recete_med)));

        RegisterAdapters registerAdapters = new RegisterAdapters(self,list);
        rv_content.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(self);
        rv_content.setLayoutManager(linearLayoutManager);
        rv_content.setAdapter(registerAdapters);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }

        return super.onOptionsItemSelected(item);
    }
}