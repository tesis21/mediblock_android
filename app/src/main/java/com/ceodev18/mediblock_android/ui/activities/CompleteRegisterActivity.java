package com.ceodev18.mediblock_android.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatEditText;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.ceodev18.mediblock_android.R;
import com.ceodev18.mediblock_android.core.login.LoginInteractor;
import com.ceodev18.mediblock_android.core.login.LoginPresenter;
import com.ceodev18.mediblock_android.core.register.RegisterInteractor;
import com.ceodev18.mediblock_android.core.register.RegisterPresenter;
import com.ceodev18.mediblock_android.core.register.RegisterView;
import com.ceodev18.mediblock_android.models.views.RegisterBody;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.ResponseBody;

public class CompleteRegisterActivity extends AppCompatActivity implements RegisterView {
    private Context context = this;

    @BindView(R.id.et_name) AppCompatEditText et_name;
    @BindView(R.id.et_lastname) AppCompatEditText et_lastname;
    @BindView(R.id.et_dni) AppCompatEditText et_dni;
    @BindView(R.id.et_phone) AppCompatEditText et_phone;


    @BindView(R.id.pb_register) ProgressBar pb_register;

    RegisterPresenter registerPresenter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_complete_register);
        ButterKnife.bind(this);
        registerPresenter = new RegisterInteractor(this, context);
    }
    private boolean is_validated(){
        return true; 
    }
    public void doRegister(View view) {

        RegisterBody registerBody = new RegisterBody();
        registerBody.setUsername(getIntent().getStringExtra("username"));
        registerBody.setPassword(getIntent().getStringExtra("password"));
        registerBody.setLastName(et_lastname.getText().toString());
        registerBody.setFirstName(et_name.getText().toString());
        registerBody.setEmail(getIntent().getStringExtra("username"));
        registerBody.setDni(et_dni.getText().toString());
        registerBody.setPhone(et_phone.getText().toString());
        Log.e("TAG REGISTER",registerBody.toString());
        if(is_validated()){
            //TODO fix
            registerPresenter.registerUser(registerBody);
        }
    }

    @Override
    public void emptyUsername() {

    }

    @Override
    public void validUsername() {

    }

    @Override
    public void emptyPassword() {

    }

    @Override
    public void validPassword() {

    }

    @Override
    public void loginSuccess() {
        pb_register.setVisibility(View.GONE);
    }

    @Override
    public void showProgress() {
        pb_register.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        pb_register.setVisibility(View.GONE);
    }

    @Override
    public void showError(ResponseBody responseBody) {

    }

    @Override
    public void errorConnection() {
        Toast.makeText(context, "Erro de Conexión", Toast.LENGTH_SHORT).show();
    }
}