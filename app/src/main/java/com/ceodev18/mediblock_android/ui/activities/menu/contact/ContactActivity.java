package com.ceodev18.mediblock_android.ui.activities.menu.contact;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.ceodev18.mediblock_android.R;
public class ContactActivity extends AppCompatActivity {
    private String m_Text = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact);
    }

    public void showForm(View view) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Ingrese los datos de su Contacto");

        // Set up the input

        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        layoutParams.setMargins(8,8,8,8);

        LinearLayout parent = new LinearLayout(this);

        parent.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        parent.setOrientation(LinearLayout.VERTICAL);

        final EditText input_dni = new EditText(this);
        input_dni.setHint("Ingrese el DNI");
        // Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
        input_dni.setInputType(InputType.TYPE_CLASS_TEXT);
        input_dni.setTextColor(getColor(R.color.colorPrimary));


        final EditText input_name = new EditText(this);
        input_name.setHint("Ingrese los nombres");
        // Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
        input_dni.setInputType(InputType.TYPE_CLASS_TEXT);
        input_name.setTextColor(getColor(R.color.colorPrimary));

        parent.addView(input_dni,layoutParams);
        parent.addView(input_name,layoutParams);

        builder.setView(parent);

        // Set up the buttons
        builder.setPositiveButton("Registrar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //m_Text = input.getText().toString();
            }
        });
        builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }
}