package com.ceodev18.mediblock_android.ui.activities.menu.dashboard;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ceodev18.mediblock_android.R;
import com.ceodev18.mediblock_android.adapters.RegisterAdapters;
import com.ceodev18.mediblock_android.models.Register;
import com.ceodev18.mediblock_android.ui.activities.MenuActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DashboardFragment extends Fragment {

    private DashboardViewModel dashboardViewModel;

    @BindView(R.id.rv_content) RecyclerView rv_content;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        dashboardViewModel =
                new ViewModelProvider(this).get(DashboardViewModel.class);
        View root = inflater.inflate(R.layout.fragment_dashboard, container, false);
        ButterKnife.bind(this,root);
        setContent();
        return root;
    }

    private void setContent(){
        MenuActivity menuActivity = (MenuActivity)getActivity();
        menuActivity.setBg(getResources().getColor(R.color.colorWhite));
        List<Register>list = new ArrayList<>();
        list.add(new Register("Cita Médica","Clínica Limatambo","29/07/202",getResources().getDrawable(R.drawable.ic_cita_med)));
        list.add(new Register("Registro Médico","Hospital San Juan","09/02/202",getResources().getDrawable(R.drawable.ic_recete_med)));

        RegisterAdapters registerAdapters = new RegisterAdapters(getContext(),list);
        rv_content.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        rv_content.setLayoutManager(linearLayoutManager);
        rv_content.setAdapter(registerAdapters);
    }
}