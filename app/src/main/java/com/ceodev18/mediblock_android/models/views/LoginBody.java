package com.ceodev18.mediblock_android.models.views;

import com.ceodev18.mediblock_android.models.User;

import lombok.Getter;
import lombok.Setter;


public class LoginBody {
    private User user;
    private String token;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
