package com.ceodev18.mediblock_android.models;

import com.ceodev18.mediblock_android.models.views.InstitutionsResponse;

import java.util.List;

public class Hospital {
    public Hospital(){

    }
    public Hospital(InstitutionsResponse institutionsResponse){
        this.name = institutionsResponse.getName();
        this.email = institutionsResponse.getEmail();
        this.address = institutionsResponse.getAddress();
        this.type = 1;
        this.agreemnent_date = "09/07/2020";
        this.category = "Clínica";
    }
    public Hospital(String agreemnent_date,String name,int type, String category){
        this.agreemnent_date = agreemnent_date;
        this.name = name;
        this.type = type;
        this.category = category;
    }
    private String agreemnent_date;
    private String name;
    private String email;
    private String address;
    private int type;
    private String category;
    private List<Register> registers;

    public String getAgreemnent_date() {
        return agreemnent_date;
    }

    public void setAgreemnent_date(String agreemnent_date) {
        this.agreemnent_date = agreemnent_date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public List<Register> getRegisters() {
        return registers;
    }

    public void setRegisters(List<Register> registers) {
        this.registers = registers;
    }
}
