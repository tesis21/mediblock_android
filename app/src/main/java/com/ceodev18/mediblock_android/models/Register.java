package com.ceodev18.mediblock_android.models;

import android.graphics.drawable.Drawable;

public class Register {
    private String type;
    private String centro;
    private String date;
    private Drawable img;
    public Register(String type, String centro,String date,Drawable img){
        this.type = type;
        this.centro = centro;
        this.date = date;
        this.setImg(img);
    }

    public Register(){

    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCentro() {
        return centro;
    }

    public void setCentro(String centro) {
        this.centro = centro;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Drawable getImg() {
        return img;
    }

    public void setImg(Drawable img) {
        this.img = img;
    }
}
